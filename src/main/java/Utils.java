import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

final class Utils {

    private final static Logger logger = LoggerFactory.getLogger(Utils.class);

    private Utils() {
    }

    /**
     * @param oldFileFullPath {@link Path Path} path from
     * @param newFileFullPath {@link String String} path to
     * @param rename if true then if target file exist source file will be renamed, otherwise target file will be
     *               removed
     * @return true - if file moved, false - otherwise
     */
    static boolean moveFile(Path oldFileFullPath, String newFileFullPath, boolean rename) {
        String finalFilePath = newFileFullPath;
        if (rename) {
            finalFilePath = incrementFileNameIfExist(0, finalFilePath);
        } else {
            boolean fileDeleted = deleteFileIfExist(finalFilePath);
            if (fileDeleted) {
                logger.info("Old existed File {} is successfully deleted.", finalFilePath);
            }
        }
        boolean fileMoved = oldFileFullPath.toFile().renameTo(new File(finalFilePath));
        if (fileMoved) {
            logger.info("File {} is successfully moved into {}", oldFileFullPath, finalFilePath);
        } else {
            logger.info("File {} wasn't moved into {}", oldFileFullPath, finalFilePath);
        }
        return fileMoved;
    }

    private static boolean deleteFileIfExist(String finalFilePath) {
        Path path = Paths.get(finalFilePath);
        if (Files.exists(path)) {
            return path.toFile().delete();
        }
        return false;
    }

    private static String incrementFileNameIfExist(int index, String finalFilePath) {
        String newFilePath = finalFilePath;
        if (Files.exists(Paths.get(finalFilePath))) {
            int dotIndex = finalFilePath.lastIndexOf(".");
            int leftBracket = finalFilePath.lastIndexOf("(");
            int rightBracket = finalFilePath.lastIndexOf(")");
            if (newFilePath.matches(".*\\(\\d\\)..*")) {
                int oldIndex = Integer.valueOf(finalFilePath.substring(leftBracket + 1, rightBracket));
                newFilePath = newFilePath.substring(0, leftBracket) + "(" + ++oldIndex + ")"
                        + newFilePath.substring(rightBracket + 1);
                return incrementFileNameIfExist(oldIndex, newFilePath);
            } else {
                newFilePath = newFilePath.substring(0, dotIndex) + "(" + ++index + ")"
                        + newFilePath.substring(dotIndex);
                return incrementFileNameIfExist(index, newFilePath);
            }
        }
        return newFilePath;
    }

    static String getMimeType(Path path) {
        try {
            return Files.probeContentType(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    static void createDirectoryIfNotExist(Path path) {
        try {
            if (Files.exists(path)) {
                return;
            }
            Files.createDirectory(path);
            logger.info("Directory {} created.", path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
