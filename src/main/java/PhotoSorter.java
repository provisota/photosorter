import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifDirectoryBase;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.file.FileSystemDirectory;
import com.drew.metadata.mp4.Mp4Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class PhotoSorter {

    private final static Logger logger = LoggerFactory.getLogger(PhotoSorter.class);
    private final String currentPath;
    private Map<String, List<Path>> newPhotoFolderToOldPathMap = new HashMap<>();
    private Map<String, List<Path>> newVideoFolderToOldPathMap = new HashMap<>();
    private final static String NO_DATE_FOLDER_NAME = "noDate";
    private final static String PHOTO_FOLDER_PREFIX = "photo\\";
    private final static String VIDEO_FOLDER_PREFIX = "video\\";
    private static List<Integer> exifSubDataTags = Arrays.asList(ExifSubIFDDirectory.TAG_DATETIME,
            ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL, ExifSubIFDDirectory.TAG_DATETIME_DIGITIZED);
    private static List<Integer> exifBaseDataTags = Arrays.asList(ExifDirectoryBase.TAG_DATETIME,
            ExifDirectoryBase.TAG_DATETIME_ORIGINAL, ExifDirectoryBase.TAG_DATETIME_DIGITIZED);
    private static Map<Class<? extends Directory>, List<Integer>> directoryToTagsList = new HashMap<>();
    private List<Integer> mp4DataTags = Arrays.asList(Mp4Directory.TAG_CREATION_TIME,
            Mp4Directory.TAG_MODIFICATION_TIME);
    private boolean photoFolderCreated = false;
    private boolean videoFolderCreated = false;
    private static boolean rename;
    private static int movedVideoCounter;
    private static int movedPhotoCounter;

    static {
        directoryToTagsList.put(ExifSubIFDDirectory.class, exifSubDataTags);
        directoryToTagsList.put(ExifDirectoryBase.class, exifBaseDataTags);
    }

    private PhotoSorter() throws IOException {
        currentPath = new File(".").getCanonicalPath();
//        currentPath = new File("D:\\test").getCanonicalPath();
    }

    public static void main(String[] args) throws IOException {
        showRemoveOrRenameDialog();
        PhotoSorter instance = new PhotoSorter();
        logger.info("Current path: {}", instance.currentPath);
        instance.fillFilesMap();
        instance.createNewFoldersIfNotExist();
        instance.moveFiles();
        printSummary();
    }

    private static void showRemoveOrRenameDialog() {
        String[] options = {"REPLACE", "RENAME", "CANCEL"};
        int dialogResult = JOptionPane.showOptionDialog(null,
                "If target file exist would you like replace it or to rename the new file?", "REPLACE or RENAME",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, "REPLACE");
        if (dialogResult == -1 || dialogResult == 2) {
            logger.info("Bye!");
            System.exit(0);
        }
        rename = dialogResult == JOptionPane.NO_OPTION;
    }

    private void moveFiles() {
        newPhotoFolderToOldPathMap.forEach((newFolderName, listOfOldPaths) -> listOfOldPaths
                .forEach(oldFileFullPath -> {
                    String newFileFullPath = getNewPhotoFileFullPath(newFolderName, oldFileFullPath);
                    boolean fileMoved = Utils.moveFile(oldFileFullPath, newFileFullPath, rename);
                    if (fileMoved) {
                        movedPhotoCounter++;
                    }
                }));
        newVideoFolderToOldPathMap.forEach((newFolderName, listOfOldPaths) -> listOfOldPaths
                .forEach(oldFileFullPath -> {
                    String newFileFullPath = getNewVideoFileFullPath(newFolderName, oldFileFullPath);
                    boolean fileMoved = Utils.moveFile(oldFileFullPath, newFileFullPath, rename);
                    if (fileMoved) {
                        movedVideoCounter++;
                    }
                }));
    }

    private void createNewFoldersIfNotExist() throws IOException {
        createParentFolders();
        newPhotoFolderToOldPathMap.keySet().stream()
                .map(this::getNewPhotoFolderFullPath)
                .map(Paths::get)
                .forEach(Utils::createDirectoryIfNotExist);
        newVideoFolderToOldPathMap.keySet().stream()
                .map(this::getNewVideoFolderFullPath)
                .map(Paths::get)
                .forEach(Utils::createDirectoryIfNotExist);
    }

    private void createParentFolders() {
        if (!photoFolderCreated && !newPhotoFolderToOldPathMap.isEmpty()) {
            Utils.createDirectoryIfNotExist(Paths.get(String.format("%s\\%s", currentPath, PHOTO_FOLDER_PREFIX)));
            photoFolderCreated = true;
        }
        if (!videoFolderCreated && !newVideoFolderToOldPathMap.isEmpty()) {
            Utils.createDirectoryIfNotExist(Paths.get(String.format("%s\\%s", currentPath, VIDEO_FOLDER_PREFIX)));
            videoFolderCreated = true;
        }
    }

    private void fillFilesMap() throws IOException {
        newPhotoFolderToOldPathMap = Files.list(Paths.get(currentPath))
                .filter(Files::isRegularFile)
                .filter(path -> {
                    String mimeType = Utils.getMimeType(path);
                    if (mimeType != null) {
                        String fileType = mimeType.split("/")[0];
                        return "image".equals(fileType);
                    }
                    return false;
                })
                .collect(Collectors.groupingBy(this::getNewFolderName));

        newVideoFolderToOldPathMap = Files.list(Paths.get(currentPath))
                .filter(Files::isRegularFile)
                .filter(path -> {
                    String mimeType = Utils.getMimeType(path);
                    if (mimeType != null) {
                        String fileType = mimeType.split("/")[0];
                        return "video".equals(fileType);
                    }
                    return false;
                })
                .collect(Collectors.groupingBy(this::getNewFolderName));
    }

    private String getNewFolderName(Path path) {
        String newFolderName = NO_DATE_FOLDER_NAME;
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(path.toFile());
            Date date = getCreateDate(metadata);

            if (date != null) {
                // Read the date
                DateFormat df = DateFormat.getDateInstance();
                df.format(date);

                int year = df.getCalendar().get(Calendar.YEAR);
                int month = df.getCalendar().get(Calendar.MONTH) + 1;

                newFolderName = String.format("%s-%s", year, (month < 10 ? "0" + month : month));
            }
            return newFolderName;
        } catch (ImageProcessingException | IOException e) {
            e.printStackTrace();
        }
        return newFolderName;
    }

    /**
     * Read Exif Data
     *
     * @param metadata - image metadata
     * @return Date of creation
     */
    private Date getCreateDate(Metadata metadata) {
        Date result = null;
        // photo date of taken
        Directory directory;
        for (Map.Entry<Class<? extends Directory>, List<Integer>> entry : directoryToTagsList.entrySet()) {
            directory = metadata.getFirstDirectoryOfType(entry.getKey());
            if (directory != null) {
                for (Integer tag : entry.getValue()) {
                    result = directory.getDate(tag);
                    if (result != null) {
                        return result;
                    }
                }
            }
        }
        // video date of taken
        directory = metadata.getFirstDirectoryOfType(Mp4Directory.class);
        if (directory != null) {
            for (Integer tag : mp4DataTags) {
                result = directory.getDate(Mp4Directory.TAG_CREATION_TIME);
                if (result != null) {
                    return result;
                }
            }
        }
        // file date of modified
        directory = metadata.getFirstDirectoryOfType(FileSystemDirectory.class);
        if (directory != null) {
            result = directory.getDate(FileSystemDirectory.TAG_FILE_MODIFIED_DATE);
        }
        return result;
    }

    private String getNewPhotoFolderFullPath(String newFolderName) {
        return String.format("%s\\%s%s", currentPath, PHOTO_FOLDER_PREFIX, newFolderName);
    }

    private String getNewVideoFolderFullPath(String newFolderName) {
        return String.format("%s\\%s%s", currentPath, VIDEO_FOLDER_PREFIX, newFolderName);
    }

    private String getNewPhotoFileFullPath(String newFolderName, Path oldPath) {
        return getNewPhotoFolderFullPath(newFolderName) + "\\" + oldPath.getFileName();
    }

    private String getNewVideoFileFullPath(String newFolderName, Path oldPath) {
        return getNewVideoFolderFullPath(newFolderName) + "\\" + oldPath.getFileName();
    }

    private static void printSummary() {
        logger.info("----------------------------------------------------------------");
        logger.info("Summary PHOTO files moved: {}", movedPhotoCounter);
        logger.info("Summary VIDEO files moved: {}", movedVideoCounter);
        logger.info("----------------------------------------------------------------");
    }
}
